


```bash
cd services
subo build .

cd ..
docker-compose up
```

```bash
data='{"key":"jane","value":"Jane Doe"}'
curl -d "${data}" \
   -H "Content-Type: application/json" \
   -X POST "http://localhost:8080/create"

curl http://localhost:8080/read/jane
```

```bash
docker exec -it redis-server /bin/sh
redis-cli
keys *

```