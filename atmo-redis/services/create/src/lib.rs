use suborbital::runnable::*;
use suborbital::cache;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct RequestMessage {
    key: String,
    value: String
}

struct Create{}

impl Runnable for Create {
    fn run(&self, input: Vec<u8>) -> Result<Vec<u8>, RunErr> {
        let in_string = String::from_utf8(input).unwrap();
        
        // read the JSON message
        let request_message: RequestMessage = serde_json::from_str(&in_string).unwrap();

        // create the key/value record
        cache::set(&request_message.key, request_message.value.as_bytes().to_vec(), 0);
    
        Ok(String::from(format!("Record key:{} value:{}\n", request_message.key, request_message.value)).as_bytes().to_vec())

    }
}


// initialize the runner, do not edit below //
static RUNNABLE: &Create = &Create{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
