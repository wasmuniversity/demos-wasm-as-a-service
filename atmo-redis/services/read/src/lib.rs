use suborbital::runnable::*;
use suborbital::req;
use suborbital::cache;

struct Read{}

impl Runnable for Read {
    fn run(&self, _: Vec<u8>) -> Result<Vec<u8>, RunErr> {

        let key = req::url_param("key");

        let value = cache::get(key.as_str()).unwrap_or_default();
        let string_value = String::from_utf8(value).unwrap();
        
        Ok(String::from(format!("hello {}", string_value)).as_bytes().to_vec())
    }

}


// initialize the runner, do not edit below //
static RUNNABLE: &Read = &Read{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
