# demos-wasm-as-a-service

## Demos Wagi

```bash
cd wagi
wagi -c modules.toml 
```

> GET
```bash
curl http://localhost:3000/hello
```

> GET query string
```bash
curl http://localhost:3000/hey?message=hello
```

> POST
```bash
curl -d "👋 hello people 😃" http://localhost:3000/hey 
```

## Demos Sat

### Hello Swift

```bash
cd sat
subo create runnable hello-swift --lang swift
# show project
# update the code

# build
subo build hello-swift/

# launch sat
SAT_HTTP_PORT=8080 sat ./hello-swift/hello-swift.wasm 
```

> Call the service
```bash
curl -d "Bob Morane" \
    -X POST "http://localhost:8080" \
    ;echo ""
```

### Hello Go

```bash
cd sat
subo create runnable hello-go --lang tinygo
# show project
# update the code
```

#### Code sample (dont't forget to run node.js)

```golang
package main

import (
	"github.com/suborbital/reactr/api/tinygo/runnable" 
	reactrHttp "github.com/suborbital/reactr/api/tinygo/runnable/http"

)

type HelloGo struct{}

func (h HelloGo) Run(input []byte) ([]byte, error) {
	queryResult, _ := reactrHttp.GET("http://localhost:8080", nil)

	return []byte(string(queryResult) + "Hello, " + string(input)), nil
}

// initialize runnable, do not edit //
func main() {
	runnable.Use(HelloGo{})
}

```

#### Build and run

```bash
# build
subo build hello-go/

# launch sat
SAT_HTTP_PORT=8081 sat ./hello-go/hello-go.wasm 
```

> Call the service
```bash
curl -d "Bill Ballantine" \
    -X POST "http://localhost:8081" \
    ;echo ""
```

### Hello Rust

```bash
cd sat
subo create runnable hello-rust
# show project
# update the code
```

#### Update dependencies and source code

```toml
[dependencies]
suborbital = '0.15.1'
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
```


```rust
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct RequestMessage {
    name: String
}

let request_message: RequestMessage = serde_json::from_str(&in_string).unwrap();

// Then replace in_string by
request_message.name
```


```bash
# build
subo build hello-rust/

# launch sat
SAT_HTTP_PORT=8082 sat ./hello-rust/hello-rust.wasm 
```

> Call the service
```bash
data='{"name":"Jane Doe"}'
curl -d "${data}" \
    -H "Content-Type: application/json" \
    -X POST "http://localhost:8082" \
    ;echo ""
```

### Hello JavaScript

- Run JavaScript inside Webassembly: https://github.com/Shopify/javy

```bash
cd sat
subo create runnable hello-js --lang javascript
# show project
# update the code

# build
subo build hello-js/

# launch sat
SAT_HTTP_PORT=8083 sat ./hello-js/hello-js.wasm 
```

> Call the service
```bash
data='{"name":"Jane Doe"}'
curl -d "${data}" \
    -H "Content-Type: application/json" \
    -X POST "http://localhost:8083" \
    ;echo ""
```

## Demos Atmo

```bash
cd atmo
# generate project
subo create project services-demo

# build
cd services-demo
subo build .

# serve
ATMO_HTTP_PORT=8080 atmo

# query 
curl http://localhost:8080/hello -d 'Jane Doe'; echo ""

```

### Add a ne service

```bash
subo create runnable hey --lang swift
```

```yaml
  - type: request
    resource: /hey
    method: POST
    steps:
      - fn: hey
```

```bash
subo build .

# serve
ATMO_HTTP_PORT=8080 atmo

# query 
curl http://localhost:8080/hello -d 'Jane Doe'; echo ""
curl http://localhost:8080/hey -d 'John Doe'; echo ""
```
