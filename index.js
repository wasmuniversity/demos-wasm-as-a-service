const http = require('http')

const requestListener = function (req, res) {
  console.log("👋 request from wasm module")
  res.writeHead(200);
  res.end("👋 Hello World 🌍")
}

console.log("🌍 listening on 8080")
const server = http.createServer(requestListener)
server.listen(8080)
